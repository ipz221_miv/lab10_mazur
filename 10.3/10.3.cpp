#include <stdio.h>
#include <Windows.h>
int main() {
	int a, choice;

	do {
		printf("Let's start this program! 1 - yes, 2 - no\n");
		scanf_s("%d", &choice);
		switch (choice) {
		case 1:
			printf("Enter the number of seconds for timer:\n");
			scanf_s("%d", &a);
			Sleep(a * 1000);
			printf("\a System sound_)\n");
			break;
		case 2:
			printf("The program has ended\n");
			break;
		default:
			printf("Pls, enter either 1 or 2!\n");
			break;
		}
	} while (choice != 2);

	return 0;
}
